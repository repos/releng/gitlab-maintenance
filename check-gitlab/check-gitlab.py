#!/usr/bin/python3

import argparse
import requests
import paramiko
import gitlab
from gitlab.exceptions import GitlabAuthenticationError
import os

def check_ssh(instance):
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        client.connect(instance, port=22, username='git')
        print(u'\u2713' + ' SSH check successful: ' + client._transport.remote_version)
    except Exception as e:
        print(u'\u2717' + ' SSH check failed: connection over SSH not possible on ' + instance)
        print(e)
    finally:
        if client:
            client.close()

def check_web(instance):
    response = requests.head('https://' + instance + "/explore")
    if response.status_code == 200:
        print( u'\u2713' + ' Web check successful: ' + str(response.status_code) + ' ' + response.headers['Date'])
    else:
        print( u'\u2717' + ' Web check failed: ' + response.headers)

def check_api(instance):
    try:
        gl = gitlab.Gitlab('https://' + instance, private_token=os.environ['GITLAB_TOKEN'])
        gl.auth()
        project = gl.projects.get(1)
        if project:
            print( u'\u2713' + ' API check successful: Project found ' + project.name_with_namespace)
        else:
            raise Exception
    except GitlabAuthenticationError as e:
        print( u'\u2717' + ' API check failed ' + str(e))
        print('Make sure to export a valid GITLAB_TOKEN')
    except Exception as e:
        print( u'\u2717' + ' API check failed')

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("instance", help="URL to GitLab instance")
    parser.add_argument("--check", default='all', choices=['all', 'http', 'api', 'ssh'], help="type of check to execute")
    parser.add_argument("--watch", action='store_true', help="continue running the checks")
    args = parser.parse_args()

    instance = args.instance

    while True:
        if args.check == 'http' or args.check == 'all':
            check_web(instance)
        if args.check == 'ssh' or args.check == 'all':
            check_ssh(instance)
        if args.check == 'api' or args.check == 'all':
            check_api(instance)
        if not args.watch:
            break

if __name__ == "__main__":
    main()
