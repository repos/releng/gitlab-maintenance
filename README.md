# GitLab maintenance script

This scripts help to automate GitLab maintenance. Available scripts are:
 * `check-gitlab.py`: intended to do blackbox-checks after upgrades or migrations.

Create virtual environment (optional):
```
python3 -m venv gitlab-maintenance
source gitlab-maintenance/bin/activate
```

## `check-gitlab.py`

### Setup
Change to `check-gitlab` directory:
```
cd check-gitlab
```

Install required packages:
```
pip install -r requirements.txt
```

Export a valid api token:
```
export GITLAB_TOKEN="your_api_token"
```

Export path to ssh auth socks (example below for `wmf-sre-laptop` package users):
```
export SSH_AUTH_SOCK=/run/user/1000/ssh-wmf-cloud.socket
```

### Usage

Use `--help` to see all parameters:
```
main.py --help
usage: main.py [-h] [--check {all,http,api,ssh}] [--watch] instance

positional arguments:
  instance              URL to GitLab instance

optional arguments:
  -h, --help            show this help message and exit
  --check {all,http,api,ssh}
                        type of check to execute
  --watch               continue running the checks
```

Example usage:
```
./check-gitlab.py gitlab.devtools.wmcloud.org
✓ Web check successful: 200 Wed, 09 Mar 2022 09:20:13 GMT
✓ SSH check successful: SSH-2.0-OpenSSH_7.9p1 Debian-10+deb10u2
✓ API check successful: Project found Jelto / test-project
```
